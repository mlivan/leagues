# League API

### Docker

Need to install docker and docker-compose

After installation:

	$ docker-compose build
	$ docker-compose up -d

and docker containers are ready
 
### Folder permission and parameters

Copy parameters: 

    cp config/parameters.yml.disc config/parameters.yml
    cp .env env
    cp phpunit.xml.dist phpunit.xml
 
## Description

Api for League API website.

#### Set up data

##### Install dependencies and edit configuration:

Into docker php container (leagues_php7) start:

    composer install

##### Out of docker container (into project folder) start this command

	sudo chown <your_user>:<your_user> -R ./*
	
##### Next command execute into docker container (leagues_php7) 

Create new database if not exist, 

    $ rdb
    
Insert structure and clear cache:

    $ cc

At the end add into /eth/hosts file (out of docker container)
	
	127.0.0.1   leagues-dev.com
	
Done!
	
#### The API end point are on following url:

http://leagues-dev.com/api/doc/

Unit Tests can be run by the command into docker container (leagues_php7)

    bin/phpunit
 