<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * Trait SluggableEntity
 */
trait SluggableEntity
{
    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"}, updatable=true)
     * @ORM\Column(length=100, unique=true, nullable=true)
     */
    private $slug;

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
