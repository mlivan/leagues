<?php

namespace App\Entity\League;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="league__match")
 *   @UniqueEntity(
 *     fields={"homeTeam", "awayTeam"}, message="Duplicated entry."
 *   )
 * @ORM\Entity(repositoryClass="App\Repository\MatchRepository")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Match
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\Group", inversedBy="matches")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\League", inversedBy="matches")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    protected $leagueTitle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\Team")
     * @ORM\JoinColumn(name="home_team_id", referencedColumnName="id")
     */
    protected $homeTeam;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\Team")
     * @ORM\JoinColumn(name="away_team_id", referencedColumnName="id")
     */
    protected $awayTeam;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose()
     */
    private $matchday;

    /**
     * @ORM\Column(type="datetime")
     *
     * @JMS\Expose()
     */
    private $kickoffAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMS\Expose()
     */
    private $score;

    /**
     * Team constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getLeagueTitle()
    {
        return $this->leagueTitle ? $this->leagueTitle->getTitle() : null;
    }

    /**
     *
     * @return mixed
     */
    public function getLeague()
    {
        return $this->leagueTitle;
    }


    /**
     * @param mixed $leagueTitle
     */
    public function setLeagueTitle($leagueTitle): void
    {
        $this->leagueTitle = $leagueTitle;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group ? $this->group->getGroup() : null;
    }

    /**
     * @return mixed
     */
    public function getGroupObj()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group): void
    {
        $this->group = $group;
    }


    /**
     * @return string
     */
    public function getMatchday()
    {
        return $this->matchday;
    }

    /**
     * @param string $matchday
     */
    public function setMatchday(string $matchday): void
    {
        $this->matchday = $matchday;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getHomeTeam()
    {
        return $this->homeTeam ? $this->homeTeam->getTitle() : null;
    }

    /**
     * @param mixed $homeTeam
     */
    public function setHomeTeam($homeTeam): void
    {
        $this->homeTeam = $homeTeam;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getAwayTeam()
    {
        return $this->awayTeam ? $this->awayTeam->getTitle() : null;
    }

    /**
     * @param mixed $awayTeam
     */
    public function setAwayTeam($awayTeam): void
    {
        $this->awayTeam = $awayTeam;
    }

    /**
     * @return mixed
     */
    public function getKickoffAt()
    {
        return $this->kickoffAt;
    }

    /**
     * @param mixed $kickoffAt
     */
    public function setKickoffAt($kickoffAt): void
    {
        $this->kickoffAt = $kickoffAt;
    }

    /**
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param string $score
     */
    public function setScore(string $score): void
    {
        $this->score = $score;
    }

    /**
     * @return int
     */
    public function getHomeGoal()
    {
        $scoreArray = explode(':', $this->score);

        return $scoreArray[0];
    }

    /**
     * @return int
     */
    public function getAwayGoal()
    {
        $scoreArray = explode(':', $this->score);

        return $scoreArray[1];
    }
}
