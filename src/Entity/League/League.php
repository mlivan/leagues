<?php

namespace App\Entity\League;

use App\Entity\Traits\SluggableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="league__league")
 * @ORM\Entity()
 *
 * @JMS\ExclusionPolicy("all")
 */
class League
{
    use SluggableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     *
     * @JMS\Expose()
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\League\Standing", mappedBy="league", cascade={"persist", "remove"})
     *
     * @JMS\Expose()
     */
    protected $standings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\League\Match", mappedBy="leagueTitle", cascade={"persist", "remove"})
     *
     * @JMS\Expose()
     */
    protected $matches;

    /**
     * Team constructor.
     */
    public function __construct()
    {
        $this->standings = new ArrayCollection();
        $this->matches = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getStandings()
    {
        return $this->standings;
    }

    /**
     * @param mixed $standings
     */
    public function setStandings($standings): void
    {
        $this->standings = $standings;
    }
}
