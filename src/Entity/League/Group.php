<?php

namespace App\Entity\League;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="league__group")
 * @ORM\Entity()
 *
 * @JMS\ExclusionPolicy("all")
 */
class Group
{
    const GROUP_A = "A";
    const GROUP_B = "B";
    const GROUP_C = "C";
    const GROUP_D = "D";
    const GROUP_E = "E";
    const GROUP_F = "F";
    const GROUP_G = "G";
    const GROUP_H = "H";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="league_group", length=1, unique=true)
     *
     * @JMS\Expose()
     */
    protected $group;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\League\Match", mappedBy="group", cascade={"persist", "remove"})
     */
    protected $matches;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\League\Standing", mappedBy="group", cascade={"persist", "remove"})
     * @ORM\OrderBy({"rank" = "ASC"})
     *
     * @JMS\Expose()
     */
    protected $standings;

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->standings = new ArrayCollection();
        $this->matches = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->group;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @param string $group
     */
    public function setGroup(string $group): void
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getStandings()
    {
        return $this->standings;
    }

    /**
     * @param mixed $standings
     */
    public function setStandings($standings): void
    {
        $this->standings = $standings;
    }

    /**
     * @param Standing $standing
     */
    public function addStanding(Standing $standing)
    {
        if ($this->standings->contains($standing)) {
            return;
        }
        $this->standings[] = $standing;
        $standing->setGroup($this);
    }

    /**
     * @param Match $match
     */
    public function addMatch(Match $match)
    {
        if ($this->matches->contains($match)) {
            return;
        }
        $this->matches[] = $match;
        $match->setGroup($this);
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getMatchday()
    {
        $matches = $this->matches;
        $matchDayLast = 0;

        if ($matches) {
            /** @var Match[] $matches */
            foreach ($matches as $match) {
                if ($match->getMatchday() > $matchDayLast) {
                    $matchDayLast = $match->getMatchday();
                }
            }
        }

        return $matchDayLast;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getLeagueTitle()
    {
        /** @var Standing $standing */
        $standing = $this->standings[0];

        if ($standing && $standing->getLeague()) {
            return $standing->getLeague()->getTitle();
        }
        return null;
    }
}
