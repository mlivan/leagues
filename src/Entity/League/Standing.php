<?php

namespace App\Entity\League;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="league__standing")
 * @ORM\Entity()
 *
 * @JMS\ExclusionPolicy("all")
 */
class Standing
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     *
     * @JMS\Expose()
     */
    private $rank;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     *
     * @JMS\Expose()
     */
    private $goals;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     *
     * @JMS\Expose()
     */
    private $goalsAgainst;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     *
     * @JMS\Expose()
     */
    private $win;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     *
     * @JMS\Expose()
     */
    private $lose;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     *
     * @JMS\Expose()
     */
    private $draw;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\Team")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\Group", inversedBy="standings")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\League\League", inversedBy="standings")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    protected $league;


    /**
     * Team constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     */
    public function setRank(int $rank): void
    {
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @param int $goals
     */
    public function setGoals(int $goals): void
    {
        $this->goals = $goals;
    }

    /**
     * @return int
     */
    public function getGoalsAgainst()
    {
        return $this->goalsAgainst;
    }

    /**
     * @param int $goalsAgainst
     */
    public function setGoalsAgainst(int $goalsAgainst): void
    {
        $this->goalsAgainst = $goalsAgainst;
    }

    /**
     * @return int
     */
    public function getWin()
    {
        return $this->win;
    }

    /**
     * @param int $win
     */
    public function setWin(int $win): void
    {
        $this->win = $win;
    }

    /**
     * @return int
     */
    public function getLose()
    {
        return $this->lose;
    }

    /**
     * @param int $lose
     */
    public function setLose(int $lose): void
    {
        $this->lose = $lose;
    }

    /**
     * @return int
     */
    public function getDraw()
    {
        return $this->draw;
    }

    /**
     * @param int $draw
     */
    public function setDraw(int $draw): void
    {
        $this->draw = $draw;
    }

    /**
     * @return mixed
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param mixed $league
     */
    public function setLeague($league): void
    {
        $this->league = $league;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group): void
    {
        $this->group = $group;
    }



    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team ? $this->team->getTitle() : null;
    }

    /**
     * @param mixed $team
     */
    public function setTeam($team): void
    {
        $this->team = $team;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     */
    public function getGoalDifference()
    {
        return $this->goals - $this->goalsAgainst;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getPoints()
    {
        return $this->win * 3 + $this->draw;
    }

    /**
     * @JMS\VirtualProperty()
     *
     * @JMS\Expose()
     *
     * @return mixed
     */
    public function getPlayedGames()
    {
        return $this->win + $this->draw + $this->lose;
    }
}