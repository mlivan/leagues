<?php

namespace App\Service\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BaseEntityManager
 */
class BaseEntityManager implements BaseEntityManagerInterface
{
    /**
     * Entity manager.
     *
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * Name of the class.
     *
     * @var string
     */
    protected $class;

    /**
     * BaseEntityManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param string $class Class name
     */
    public function __construct(EntityManagerInterface $entityManager, $class)
    {
        $this->em = $entityManager;
        $this->class = $class;
    }

    /**
     * Create new entity.
     *
     * @return mixed
     */
    public function create()
    {
        $class = $this->em
            ->getRepository($this->class)
            ->getClassName();

        return new $class;
    }

    /**
     * Find entity by id.
     *
     * @param int $id Entity identifier
     *
     * @return null|object
     */
    public function find($id)
    {
        return $this->em
            ->getRepository($this->class)
            ->find($id);
    }

    /**
     * Find entity by slug.
     *
     * @param string $slug Entity slug
     *
     * @return null|object
     */
    public function findBySlug($slug)
    {
        return $this->em
            ->getRepository($this->class)
            ->findOneBy([
                'slug' => $slug,
            ]);
    }

    /**
     * Search entites by given criterias.
     *
     * @param array $filter List of fiels=>value pairs to filter by
     * @param array $order  List of field=>directions pairs to sort by
     * @param int   $limit  Number of entities to return
     * @param int   $offset Number of entity to start from
     *
     * @return mixed
     */
    public function findAll($filter = [], $order = [], $limit = null, $offset = null)
    {
        return $this->em
            ->getRepository($this->class)
            ->findBy(
                $filter,
                $order,
                $limit,
                $offset
            );
    }

    /**
     * Find entities by ids.
     *
     * @param array $ids
     *
     * @return array
     */
    public function findByIds(array $ids)
    {
        return $this->em
            ->getRepository($this->class)
            ->findBy([
                'id' => $ids,
            ]);
    }

    /**
     * Find one entity by given criteria
     *
     * @param array $criteria
     *
     * @return null|object
     */
    public function findOneBy($criteria)
    {
        return $this->em
            ->getRepository($this->class)
            ->findOneBy($criteria);
    }

    /**
     * Find entities by given criteria
     *
     * @param array $criteria
     * @param array $order
     *
     * @return null|object
     */
    public function findBy($criteria, $order = null)
    {
        return $this->em
            ->getRepository($this->class)
            ->findBy($criteria, $order);
    }

    /**
     * Refresh entity
     *
     * @param object $entity
     */
    public function refresh($entity)
    {
        $this->em->refresh($entity);
    }

    /**
     * Save doctrine entity
     *
     * @param object $entity
     * @param bool $refresh
     *
     * @return mixed|object
     */
    public function save($entity, $refresh = false)
    {
        $this->em->persist($entity);
        $this->em->flush();

        if ($refresh) {
            $this->refresh($entity);
        }

        return $entity;
    }

    /**
     * Delete entity.
     *
     * @param object $entity Entity to delete
     */
    public function delete($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * Save doctrine entity
     *
     * @param object $entity
     *
     * @return mixed|object
     */
    public function persist($entity)
    {
        $this->em->persist($entity);

        return $entity;
    }

    /**
     * Flush
     */
    public function flush()
    {
        $this->em->flush();
    }
}
