<?php

namespace App\Service\Manager\League;

use App\Entity\League\Group;
use App\Entity\League\League;
use App\Entity\League\Team;
use App\Service\Manager\BaseEntityManager;
use Doctrine\ORM\EntityManagerInterface;

class MatchManager extends BaseEntityManager
{
    /**
     * @var TeamManager
     */
    private $teamManager;

    /**
     * MatchManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param $class
     * @param TeamManager $teamManager
     */
    public function __construct(EntityManagerInterface $entityManager, $class, TeamManager $teamManager)
    {
        parent::__construct($entityManager, $class);
        $this->teamManager = $teamManager;
    }

    /**
     * Get teams by group and league
     *
     * @param Group $group
     * @param League $league
     *
     * @return mixed
     */
    public function getTeamsByLeagueAndGroup(League $league, Group $group)
    {
        $teamsData = $matchRepository = $this->em->getRepository($this->class)
            ->getTeamsByLeagueAndGroup($league, $group);
        $teamIds = [];

        foreach ($teamsData as $teamData) {
            $teamIds[] = $teamData['team'];
        }

        $teams = $this->teamManager->findByIds($teamIds);

        return $teams;
    }

    /**
     * Get matches by team
     *
     * @param Team $team
     * @return mixed
     */
    public function getMatchesByTeam(Team $team)
    {
        $teams =  $this->em->getRepository($this->class)->getMatchesByTeam($team);

        return $teams;
    }

    /**
     * Get match by league, group and teams
     *
     * @param string $leagueTitle
     * @param string $group
     * @param string $homeTeam
     * @param string $awayTeam
     * @return mixed
     */
    public function getMatchByLeagueGroupAndTeams(
        string $leagueTitle,
        string $group,
        string $homeTeam,
        string $awayTeam
    ) {
        $match =  $this->em->getRepository($this->class)
            ->getMatchByLeagueGroupAndTeams($leagueTitle, $group, $homeTeam, $awayTeam);

        return $match;
    }

    /**
     * Search match by filter
     *
     * @param array $filter
     * @return mixed
     */
    public function search(array $filter)
    {
        $teams =  $this->em->getRepository($this->class)->search($filter);

        return $teams;
    }
}
