<?php

namespace App\Service\Manager\League;

use App\Service\Manager\BaseEntityManager;
use Doctrine\ORM\EntityManagerInterface;

class TeamManager extends BaseEntityManager
{
    /**
     * LeagueManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param $class
     */
    public function __construct(EntityManagerInterface $entityManager, $class)
    {
        parent::__construct($entityManager, $class);
    }
}
