<?php

namespace App\Service\Manager\League;

use App\Entity\League\Group;
use App\Entity\League\League;
use App\Entity\League\Match;
use App\Entity\League\Standing;
use App\Entity\League\Team;
use App\Service\Manager\BaseEntityManager;
use App\Service\Util\Util;
use Doctrine\ORM\EntityManagerInterface;

class StandingManager extends BaseEntityManager
{
    /**
     * @var Util
     */
    private $util;

    /**
     * @var MatchManager
     */
    private $matchManager;
    /**
     * @var GroupManager
     */
    private $groupManager;
    /**
     * @var LeagueManager
     */
    private $leagueManager;

    /**
     * StandingManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param $class
     * @param Util $util
     * @param MatchManager $matchManager
     * @param GroupManager $groupManager
     * @param LeagueManager $leagueManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        $class,
        Util $util,
        MatchManager $matchManager,
        GroupManager $groupManager,
        LeagueManager $leagueManager
    ) {
        parent::__construct($entityManager, $class);
        $this->util = $util;
        $this->matchManager = $matchManager;
        $this->groupManager = $groupManager;
        $this->leagueManager = $leagueManager;
    }

    /**
     * Update standing by group and league
     *
     * @param League[] $leagues
     * @param Group[] $groups
     */
    public function updateStandingGroups(array $leagues, array $groups)
    {
        $league = $leagues[0];

        foreach ($groups as $group) {
            $standings = [];
            /** @var Team[] $teams */
            $teams = $this->matchManager->getTeamsByLeagueAndGroup($league, $group);

            foreach ($teams as $team) {
                /** @var Standing $standing */
                $standing = $this->findOneBy(['team' => $team->getId()]);

                if (!$standing) {
                    $standing = new Standing();
                    $standing->setGroup($group);
                    $standing->setLeague($league);
                    $standing->setTeam($team);
                }

                /** @var Match[] $matches */
                $matches = $this->matchManager->getMatchesByTeam($team);

                $totalGoals = $totalAgainstGoals = $wins = $lose = $draw = 0;

                foreach ($matches as $match) {
                    if ($match->getHomeTeam() == $team->getTitle()) {
                        $totalGoals = $this->util->calculateGoals($match->getHomeGoal(), $totalGoals);
                        $totalAgainstGoals = $this->util->calculateGoals($match->getAwayGoal(), $totalAgainstGoals);

                        [$wins, $lose, $draw] = $this->util
                            ->calculateMatches($match->getHomeGoal(), $match->getAwayGoal(), $wins, $lose, $draw);
                    } else {
                        $totalGoals = $this->util->calculateGoals($match->getAwayGoal(), $totalGoals);
                        $totalAgainstGoals = $this->util->calculateGoals($match->getHomeGoal(), $totalAgainstGoals);

                        [$wins, $lose, $draw] = $this->util
                            ->calculateMatches($match->getAwayGoal(), $match->getHomeGoal(), $wins, $lose, $draw);
                    }
                }

                $standing->setWin($wins);
                $standing->setLose($lose);
                $standing->setDraw($draw);

                $standing->setGoals($totalGoals);
                $standing->setGoalsAgainst($totalAgainstGoals);
                $group->addStanding($standing);

                $this->persist($group);
                $this->refresh($group);
                $this->persist($standing);

                $standings[] = $standing;
            }

            $this->calculateRank($standings);
        }

        $this->flush();
    }

    /**
     * @param Standing[] $standings
     *
     * @return Standing[]|array
     */
    private function calculateRank(array $standings)
    {
        usort($standings, array($this, "rankSort"));

        for ($i = 0; $i < sizeof($standings); $i++) {
            $standings[$i]->setRank($i+1);
            $this->save($standings[$i]);
        }

        return $standings;
    }

    /**
     * Sort Rank
     *
     * @param Standing $a
     * @param Standing $b
     * @return int
     */
    private function rankSort(Standing $a, Standing $b)
    {
        if ($a->getPoints() == $b->getPoints()) {
            if ($a->getGoals() == $b->getGoals()) {
                if ($a->getGoalDifference() == $b->getGoals()) {
                    return 0;
                }
                return $a->getGoalDifference() < $b->getGoalDifference() ? 1 : -1;
            }
            return $a->getGoals() < $b->getGoals() ? 1 : -1;
        }
        return $a->getPoints() < $b->getPoints() ? 1 : -1;
    }
}
