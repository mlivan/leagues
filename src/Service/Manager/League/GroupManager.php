<?php

namespace App\Service\Manager\League;

use App\Service\Manager\BaseEntityManager;
use Doctrine\ORM\EntityManagerInterface;

class GroupManager extends BaseEntityManager
{
    /**
     * TeamManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param $class
     */
    public function __construct(EntityManagerInterface $entityManager, $class)
    {
        parent::__construct($entityManager, $class);
    }
}
