<?php

namespace App\Service\Util;

class Util
{
    /**
     * Calculate goals by team
     *
     * @param $goals
     * @param $totalGoals
     * @return mixed
     */
    public function calculateGoals($goals, &$totalGoals)
    {
        $totalGoals = $totalGoals + $goals;
        return $totalGoals;
    }

    /**
     * Calculate matches
     *
     * @param $giveGoals
     * @param $receivedGoals
     * @param $wins
     * @param $lose
     * @param $draw
     * @return mixed
     */
    public function calculateMatches($giveGoals, $receivedGoals, &$wins, &$lose, &$draw)
    {
        if ($giveGoals > $receivedGoals) {
            $wins++;
        } elseif ($giveGoals == $receivedGoals) {
            $draw++;
        } elseif ($giveGoals < $receivedGoals) {
            $lose++;
        }

        return [$wins, $lose, $draw];
    }

    /**
     * Return objects by reference
     *
     * @param $object
     * @param $objects
     * @return array
     */
    public function getObjectList($object, &$objects)
    {
        if (!in_array($object, $objects)) {
            $objects[] = $object;
        }
        return $objects;
    }
}
