<?php

namespace App\Repository;

use App\Entity\League\Group;
use App\Entity\League\League;
use App\Entity\League\Team;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityRepository;

class MatchRepository extends EntityRepository
{
    /**
     * Get teams by league and group
     *
     * @param League $league
     * @param Group $group
     *
     * @return mixed[]|null
     */
    public function getTeamsByLeagueAndGroup(League $league, Group $group)
    {
        try {
            $connection = $this->getEntityManager()->getConnection();

            $statement = $connection->prepare(
                "SELECT DISTINCT m.home_team_id AS team
            FROM league__match AS m
            WHERE m.group_id = :group AND m.league_id = :league
            UNION
            SELECT DISTINCT m.away_team_id AS team
            FROM league__match AS m
            WHERE m.group_id = :group AND m.league_id = :league"
            );

            $params = array(
                'league' => $league->getId(),
                'group' => $group->getId()
            );

            $statement->execute($params);

            $results = $statement->fetchAll();

            return ($results) ? $results : null;
        } catch (DBALException $e) {
            return null;
        }
    }

    /**
     * Get matches by team
     *
     * @param Team $team
     * @return mixed|null
     */
    public function getMatchesByTeam(Team $team)
    {
        $sql = "SELECT m
                FROM App\Entity\League\Match m
                WHERE m.homeTeam = :team OR m.awayTeam = :team
                ";

        $result = $this->getEntityManager()
            ->createQuery($sql)
            ->setParameter("team", $team)
            ->getResult();

        return ($result) ? $result : null;
    }

    /**
     * Get matches by team
     *
     * @param string $leagueTitle
     * @param string $group
     * @param string $homeTeam
     * @param string $awayTeam
     * @return mixed|null
     */
    public function getMatchByLeagueGroupAndTeams(
        string $leagueTitle,
        string $group,
        string $homeTeam,
        string $awayTeam
    ) {
        $sql = "SELECT m
                FROM App\Entity\League\Match m
                WHERE m.homeTeam = :homeTeam AND m.awayTeam = :awayTeam
                AND m.leagueTitle = :leagueTitle AND m.group = :group
                ";

        $result = $this->getEntityManager()
            ->createQuery($sql)
            ->setParameter("leagueTitle", $leagueTitle)
            ->setParameter("group", $group)
            ->setParameter("homeTeam", $homeTeam)
            ->setParameter("awayTeam", $awayTeam)

            ->getResult();

        return ($result) ? $result : null;
    }

    /**
     * Search match by filter
     *
     * @param array $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function search(array $filter)
    {
        $qb = $this->createQueryBuilder('m');

        if (isset($filter['group'])) {
            $qb
                ->andWhere('m.group = :group')
                ->setParameter('group', $filter['group']);
        }

        if (isset($filter['homeTeam'])) {
            $qb
                ->andWhere('m.homeTeam = :homeTeam')
                ->setParameter('homeTeam', $filter['homeTeam']);
        }

        if (isset($filter['awayTeam'])) {
            $qb
                ->andWhere('m.awayTeam = :awayTeam')
                ->setParameter('awayTeam', $filter['awayTeam']);
        }

        if (isset($filter['kickoffAtStart'])) {
            $qb
                ->andWhere('m.kickoffAt > :kickoffAtStart')
                ->setParameter('kickoffAtStart', $filter['kickoffAtStart']);
        }

        if (isset($filter['kickoffAtEnd'])) {
            $qb
                ->andWhere('m.kickoffAt < :kickoffAtEnd')
                ->setParameter('kickoffAtEnd', $filter['kickoffAtEnd']);
        }

        $query = $qb->getQuery();
        $result = $query->execute();

        return $result;
    }
}
