<?php

namespace App\Form\Type;

use App\Entity\League\Group;
use App\Form\Transformer\GroupToNameTransformer;
use App\Form\Transformer\LeagueToNameTransformer;
use App\Form\Transformer\TeamToNameTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class MatchType extends AbstractType
{
    /**
     * @var GroupToNameTransformer
     */
    private $groupToNameTransformer;
    /**
     * @var LeagueToNameTransformer
     */
    private $leagueToNameTransformer;
    /**
     * @var TeamToNameTransformer
     */
    private $teamToNameTransformer;

    /**
     * MatchType constructor.
     *
     * @param GroupToNameTransformer $groupToNameTransformer
     * @param LeagueToNameTransformer $leagueToNameTransformer
     * @param TeamToNameTransformer $teamToNameTransformer
     */
    public function __construct(
        GroupToNameTransformer $groupToNameTransformer,
        LeagueToNameTransformer $leagueToNameTransformer,
        TeamToNameTransformer $teamToNameTransformer
    ) {
        $this->groupToNameTransformer = $groupToNameTransformer;
        $this->leagueToNameTransformer = $leagueToNameTransformer;
        $this->teamToNameTransformer = $teamToNameTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder
                    ->create('group', ChoiceType::class, [
                        'required' => true,
                        "choices" => array(
                            Group::GROUP_A => Group::GROUP_A,
                            Group::GROUP_B => Group::GROUP_B,
                            Group::GROUP_C => Group::GROUP_C,
                            Group::GROUP_D => Group::GROUP_D,
                            Group::GROUP_E => Group::GROUP_E,
                            Group::GROUP_F => Group::GROUP_F,
                            Group::GROUP_G => Group::GROUP_G,
                            Group::GROUP_H => Group::GROUP_H,
                        )
                    ])
                    ->addModelTransformer($this->groupToNameTransformer)
            )
            ->add(
                $builder
                    ->create('leagueTitle', TextType::class, [
                        'required' => true
                    ])
                    ->addModelTransformer($this->leagueToNameTransformer)
            )
            ->add(
                $builder
                    ->create('homeTeam', TextType::class, [
                        'required' => true,
                    ])
                    ->addModelTransformer($this->teamToNameTransformer)
            )
            ->add(
                $builder
                    ->create('awayTeam', TextType::class, [
                        'required' => true,
                    ])
                    ->addModelTransformer($this->teamToNameTransformer)
            )

            ->add('matchday', IntegerType::class, array(
                'required' => false,
                "constraints" => array(
                    new NotNull(array(
                        "message" => "form.league.name.validation.not_null"
                    )),
                    new Length(array(
                        "max" => 100,
                        "maxMessage" => "form.league.name.validation.max",
                    ))
                )
            ))
            ->add('kickoffAt', DateTimeType::class, array(
                "required" => false,
                'widget' => 'single_text',
            ))
            ->add('score', TextType::class, array(
                'required' => true,
                "constraints" => array(
                    new NotNull(array(
                        "message" => "form.team.name.validation.not_null"
                    )),
                    new Length(array(
                        "max" => 100,
                        "maxMessage" => "form.team.name.validation.max",
                    ))
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\League\Match',
            'csrf_protection' => false,
            'translation_domain' => 'AppLeague',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }


}