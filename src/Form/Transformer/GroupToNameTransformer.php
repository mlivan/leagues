<?php

namespace App\Form\Transformer;

use App\Entity\League\Group;
use App\Service\Manager\BaseEntityManagerInterface;
use App\Service\Manager\League\GroupManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class GroupToNameTransformer
 */
class GroupToNameTransformer implements DataTransformerInterface
{
    /**
     * @var BaseEntityManagerInterface
     */
    private $groupManager;

    /**
     * GenresTransformer constructor.
     *
     * @param BaseEntityManagerInterface $groupManager
     */
    public function __construct(BaseEntityManagerInterface $groupManager)
    {
        /** @var GroupManager $groupManager */
        $this->groupManager = $groupManager;
    }

    /**
     * { @inheritdoc }
     */
    public function transform($groupName)
    {
        /** @var Group $group */
        if ($groupName) {
            $group = $this->groupManager->findOneBy(['group' => $groupName]);
            if ($group) {
                return $group;
            }
        }
        return null;
    }

    /**
     * { @inheritdoc }
     */
    public function reverseTransform($groupName)
    {
        if ($groupName) {
            $group = $this->groupManager->findOneBy(['group' => $groupName]);

            if (!$group) {
                /** @var Group $group */
                $group = $this->groupManager->create();
                $group->setGroup($groupName);

                $group = $this->groupManager->save($group);

                return $group;
            }

            return $group;
        } else {
            return null;
        }
    }
}