<?php

namespace App\Form\Transformer;

use App\Entity\League\League;
use App\Service\Manager\BaseEntityManagerInterface;
use App\Service\Manager\League\LeagueManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class LeagueToNameTransformer
 */
class LeagueToNameTransformer implements DataTransformerInterface
{
    /**
     * @var BaseEntityManagerInterface
     */
    private $leagueManager;

    /**
     * GenresTransformer constructor.
     *
     * @param BaseEntityManagerInterface $leagueManager
     */
    public function __construct(BaseEntityManagerInterface $leagueManager)
    {
        /** @var LeagueManager $leagueManager */
        $this->leagueManager = $leagueManager;
    }

    /**
     * { @inheritdoc }
     */
    public function transform($leagueTitle)
    {
        /** @var League $league */
        if ($leagueTitle) {
            $league = $this->leagueManager->findOneBy(['title' => $leagueTitle]);
            if ($league) {
                return $league;
            }
        }
        return null;
    }

    /**
     * { @inheritdoc }
     */
    public function reverseTransform($leagueTitle)
    {
        if ($leagueTitle) {
            $league = $this->leagueManager->findOneBy(['title' => $leagueTitle]);

            if (!$league) {
                /** @var League $league */
                $league = $this->leagueManager->create();
                $league->setTitle($leagueTitle);

                $league = $this->leagueManager->save($league);

                return $league;
            }

            return $league;
        } else {
            return null;
        }
    }
}