<?php

namespace App\Form\Transformer;

use App\Entity\League\Team;
use App\Service\Manager\BaseEntityManagerInterface;
use App\Service\Manager\League\TeamManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class TeamToNameTransformer
 */
class TeamToNameTransformer implements DataTransformerInterface
{
    /**
     * @var BaseEntityManagerInterface
     */
    private $teamManager;

    /**
     * GenresTransformer constructor.
     *
     * @param BaseEntityManagerInterface $teamManager
     */
    public function __construct(BaseEntityManagerInterface $teamManager)
    {
        /** @var TeamManager $teamManager */
        $this->teamManager = $teamManager;
    }

    /**
     * { @inheritdoc }
     */
    public function transform($teamTitle)
    {
        /** @var Team $team */
        if ($teamTitle) {
            $team = $this->teamManager->findOneBy(['title' => $teamTitle]);
            if ($team) {
                return $team;
            }
        }
        return null;
    }

    /**
     * { @inheritdoc }
     */
    public function reverseTransform($teamName)
    {
        if ($teamName) {
            $team = $this->teamManager->findOneBy(['title' => $teamName]);

            if (!$team) {
                /** @var Team $team */
                $team = $this->teamManager->create();
                $team->setTitle($teamName);

                $team = $this->teamManager->save($team);

                return $team;
            }

            return $team;
        } else {
            return null;
        }
    }
}