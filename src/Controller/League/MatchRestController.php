<?php

namespace App\Controller\League;

use App\Controller\BaseRestController;
use App\Entity\League\Group;
use App\Entity\League\Match;
use App\Form\Type\MatchType;
use App\Service\Manager\League\GroupManager;
use App\Service\Manager\League\LeagueManager;
use App\Service\Manager\League\MatchManager;
use App\Service\Manager\League\StandingManager;
use App\Service\Manager\League\TeamManager;
use App\Service\Util\Util;
use Doctrine\ORM\ORMException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use Symfony\Contracts\Translation\TranslatorInterface;

class MatchRestController extends BaseRestController
{
    private $matchManager;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var GroupManager
     */
    private $groupManager;
    /**
     * @var LeagueManager
     */
    private $leagueManager;
    /**
     * @var TeamManager
     */
    private $teamManager;
    /**
     * @var Util
     */
    private $util;
    /**
     * @var StandingManager
     */
    private $standingManager;

    /**
     * MatchRestController constructor.
     *
     * @param MatchManager $matchManager
     * @param GroupManager $groupManager
     * @param LeagueManager $leagueManager
     * @param TeamManager $teamManager
     * @param StandingManager $standingManager
     * @param Util $util
     * @param TranslatorInterface $translator
     */
    public function __construct(
        MatchManager $matchManager,
        GroupManager $groupManager,
        LeagueManager $leagueManager,
        TeamManager $teamManager,
        StandingManager $standingManager,
        Util $util,
        TranslatorInterface $translator
    ) {
        parent::__construct($matchManager);

        $this->matchManager = $matchManager;
        $this->translator = $translator;
        $this->groupManager = $groupManager;
        $this->leagueManager = $leagueManager;
        $this->teamManager = $teamManager;
        $this->util = $util;
        $this->standingManager = $standingManager;
    }

    /**
     * Add league with matches.
     *
     * @Route("/league/matches", methods={"POST"})
     * @SWG\Post(
     *     tags={"Matches"},
     *     summary="Add league with matches",
     *     description="Add league with matches",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(
     *           type="array",
     *           @SWG\Items(
     *            ref=@Model(type=MatchType::class)
     *          )
     *       )
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *          response=400, description="Data invalid"
     *      )
     *  )
     * @param Request $request
     *
     * @return View
     */
    public function postLeagueMatchesAction(Request $request)
    {
        $req = $request->request;
        $requestArray = $req->all();
        $leagues = $groups = [];

        for ($i = 0; $i < count($requestArray); $i++) {
            $req->remove($i);
        }

        for ($i = 0; $i < count($requestArray); $i++) {
            $req->add($requestArray[$i]);
            /** @var Match $match */
            $match = $this->matchManager->create();

            $form = $this->createForm(MatchType::class, $match, [
                'method' => Request::METHOD_POST,
                'csrf_protection' => false,
            ]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->matchManager->persist($match);
                /** @var Group $group */
                $group = $match->getGroupObj();
                $group->addMatch($match);
                $this->groupManager->persist($group);

                $groups = $this->util->getObjectList($match->getGroupObj(), $groups);
                $leagues = $this->util->getObjectList($match->getLeague(), $leagues);
            } else {
                return $this->bad('Invalid input');
            }
        }

        $this->matchManager->flush();

        $this->standingManager->updateStandingGroups($leagues, $groups);

        $groups = $this->groupManager->findAll();

        return $this->ok($groups);
    }

    /**
     * Update league matches
     *
     * @Route("/league/matches", methods={"PUT"})
     * @SWG\Put(
     *     tags={"Matches"},
     *     summary="Update league matches",
     *     description="Update league matches",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         @SWG\Schema(
     *           type="array",
     *           @SWG\Items(
     *            ref=@Model(type=MatchType::class)
     *          )
     *       )
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *          response=400, description="Data invalid"
     *      )
     *  )
     * @param Request $request
     *
     * @return View
     */
    public function putLeagueMatchAction(Request $request)
    {
        $req = $request->request;
        $requestArray = $req->all();
        $leagues = $groups = [];

        for ($i = 0; $i < count($requestArray); $i++) {
            $req->remove($i);
        }

        for ($i = 0; $i < count($requestArray); $i++) {
            $req->add($requestArray[$i]);

            $leagueTitle = $this->leagueManager->findOneBy(['title' => $req->get('leagueTitle')]);
            $group = $this->groupManager->findOneBy(['group' => $req->get('group')]);
            $homeTeam = $this->teamManager->findOneBy(['title' => $req->get('homeTeam')]);
            $awayTeam = $this->teamManager->findOneBy(['title' => $req->get('awayTeam')]);
            /** @var Match $match */
            $match = $this->matchManager
                ->findOneBy([
                    'leagueTitle' => $leagueTitle,
                    'group' => $group,
                    'homeTeam' => $homeTeam,
                    'awayTeam' => $awayTeam
                ]);

            if (!$match) {
                return $this->notFound($this->translator->trans(
                    'league.match.not_found',
                    [],
                    'AppLeague'
                ));
            }

            $form = $this->createForm(MatchType::class, $match, [
                'method' => Request::METHOD_PUT,
                'csrf_protection' => false,
            ]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->matchManager->persist($match);
                /** @var Group $group */
                $group = $match->getGroupObj();
                $group->addMatch($match);
                $this->groupManager->persist($group);

                $groups = $this->util->getObjectList($match->getGroupObj(), $groups);
                $leagues = $this->util->getObjectList($match->getLeague(), $leagues);
            } else {
                return $this->bad('Invalid input');
            }
        }

        $this->matchManager->flush();

        $this->standingManager->updateStandingGroups($leagues, $groups);

        $groups = $this->groupManager->findAll();

        return $this->ok($groups);
    }

    /**
     *  Get matches
     *
     * @Route("/league/matches", methods={"GET"})
     * @SWG\Get(
     *  tags={"Matches"},
     *     summary="Get matches",
     *     description="Get matches",
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *          name="group",
     *          in="query",
     *          description="Search by group",
     *          type="string",
     *      ),
     *      @SWG\Parameter(
     *          name="homeTeam",
     *          in="query",
     *          description="Search by homeTeam",
     *          type="string",
     *      ),
     *      @SWG\Parameter(
     *          name="awayTeam",
     *          in="query",
     *          description="Search by awayTeam",
     *          type="string",
     *      ),
     *      @SWG\Parameter(
     *          name="kickofAtStart",
     *          in="query",
     *          description="Search by kickofAtStart (format: 2017-09-13T20:45:00)",
     *          type="string",
     *      ),
     *      @SWG\Parameter(
     *          name="kickofAtEnd",
     *          in="query",
     *          description="Search by kickofAtEnd (format: 2017-09-13T20:45:00)",
     *          type="string",
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @SWG\Response(
     *          response=204, description="No content"
     *      )
     *  )
     * @param Request $request
     *
     * @return View
     * @throws \Exception
     */
    public function getLeagueMatchesAction(Request $request)
    {
        $filter = [];
        $group = $request->get('group');
        $homeTeam = $request->get('homeTeam');
        $awayTeam = $request->get('awayTeam');
        $kickoffAtStart = $request->get('kickofAtStart');
        $kickoffAtEnd = $request->get('kickofAtEnd');

        if ($group) {
            $group = $this->groupManager->findOneBy(['group' => $group]);
            $filter['group'] = $group;
        }

        if ($homeTeam) {
            $homeTeam = $this->teamManager->findOneBy(['title' => $homeTeam]);
            $filter['homeTeam'] = $homeTeam;
        }

        if ($awayTeam) {
            $awayTeam = $this->teamManager->findOneBy(['title' => $awayTeam]);
            $filter['awayTeam'] = $awayTeam;
        }

        if ($kickoffAtStart) {
            $filter['kickoffAtStart'] = new \DateTime($kickoffAtStart);
        }

        if ($kickoffAtEnd) {
            $filter['kickoffAtEnd'] = new \DateTime($kickoffAtEnd);
        }

        $matches = $this->matchManager->search($filter);

        if ($matches) {
            return $this->ok($matches);
        } else {
            return $this->noContent();
        }
    }

    /**
     * { @inheritdoc }
     */
    protected function getFormClass()
    {
        return null;
    }
}