<?php

namespace App\Controller\League;

use App\Controller\BaseRestController;
use App\Service\Manager\League\GroupManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;

class GroupRestController extends BaseRestController
{
    /**
     * @var GroupManager
     */
    private $groupManager;

    public function __construct(
        GroupManager $groupManager
    ) {
        parent::__construct($groupManager);

        $this->groupManager = $groupManager;
    }

   /**
    * Get league groups.
    *
    * @Route("/league/groups", methods={"GET"})
    * @SWG\Get(
    *   tags={"Group"},
    *   summary="Get groups",
    *   description="Get groups",
    *   produces={"application/json"},
    *   @SWG\Parameter(
    *       name="group",
    *       in="query",
    *       description="Search by group",
    *       type="string"
    *   ),
    *   @SWG\Response(
    *       response=200,
    *       description="Success"
    *   ),
    *   @SWG\Response(
    *       response=204,
    *       description="No Content"
    *   )
    * )
    * @param Request $request
    *
    * @return View
    */
    public function getGroupAction(Request $request)
    {
        $groupName = $request->get('group');
        $filter = [];

        if ($groupName) {
            $filter['group'] = $groupName;
        }

        $groups = $this->groupManager->findBy($filter);

        if ($groups) {
            return $this->ok($groups);
        }

        return $this->noContent();
    }

    /**
     * { @inheritdoc }
     */
    protected function getFormClass()
    {
        return null;
    }
}