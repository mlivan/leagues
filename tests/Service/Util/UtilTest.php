<?php

namespace App\Tests\Service\Util;

use App\Entity\League\Group;
use App\Service\Util\Util;
use PHPUnit\Framework\TestCase;

class UtilTest extends TestCase
{
    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function testCalculateGoals()
    {
        $totalGoals = 0;

        $util = new Util();
        $totalGoals = $util->calculateGoals(2, $totalGoals);
        $totalGoals = $util->calculateGoals(1, $totalGoals);
        $totalGoals = $util->calculateGoals(4, $totalGoals);

        $this->assertEquals(7, $totalGoals);
    }

    public function testCalculateMatches()
    {
        $wins = 0;
        $lose = 0;
        $draw = 0;

        $util = new Util();

        [$wins, $lose, $draw] = $util->calculateMatches(3, 2, $wins, $lose, $draw);
        [$wins, $lose, $draw] = $util->calculateMatches(1, 1, $wins, $lose, $draw);
        [$wins, $lose, $draw] = $util->calculateMatches(1, 4, $wins, $lose, $draw);
        $result = $util->calculateMatches(2, 1, $wins, $lose, $draw);

        $this->assertEquals([2, 1, 1], $result);
    }

    public function testGetObjectList()
    {
        $groups = [];

        $group1 = new Group();
        $group1->setGroup('A');

        $group2 = new Group();
        $group2->setGroup('B');

        $group3 = new Group();
        $group3->setGroup('A');

        $util = new Util();
        $groups = $util->getObjectList($group1, $groups);

        $groups = $util->getObjectList($group2, $groups);

        $this->assertEquals([$group1, $group2], $groups);
    }
}
